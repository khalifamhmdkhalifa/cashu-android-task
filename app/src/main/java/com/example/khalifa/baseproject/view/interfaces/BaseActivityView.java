package com.example.khalifa.baseproject.view.interfaces;

import android.arch.lifecycle.ViewModel;

public interface BaseActivityView<VM extends ViewModel> extends BaseView<VM> {
    void finishActivity();
}
