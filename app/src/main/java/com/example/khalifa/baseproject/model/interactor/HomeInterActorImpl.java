package com.example.khalifa.baseproject.model.interactor;

import com.example.khalifa.baseproject.model.entity.Repo;
import com.example.khalifa.baseproject.model.requestsServices.HomeService;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

public class HomeInterActorImpl extends HomeInterActor {
    private static final int ITEMS_PER_PAGE = 15;

    @Override
    public Observable<ArrayList<Repo>> getRepos(int pageNumber) {
        return applyScheduler(
                getRetroFit().create(HomeService.class).reposList(ITEMS_PER_PAGE, pageNumber));
    }
}
