package com.example.khalifa.baseproject.dependencyInjection.component;

import com.example.khalifa.baseproject.CustomApplication;
import com.example.khalifa.baseproject.dependencyInjection.module.ApplicationModule;
import com.example.khalifa.baseproject.dependencyInjection.module.InterActorsModule;
import com.example.khalifa.baseproject.dependencyInjection.module.NetworkModule;
import com.example.khalifa.baseproject.dependencyInjection.module.ViewModelModule;
import com.example.khalifa.baseproject.model.interactor.HomeInterActorImpl;
import com.example.khalifa.baseproject.presenter.HomeActivityPresenterImpl;
import com.example.khalifa.baseproject.view.activities.ActivityNavigator;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class
        , InterActorsModule.class, ViewModelModule.class, NetworkModule.class})
public interface ApplicationComponent {

    void inject(HomeActivityPresenterImpl mainActivityPresenter);

    void inject(HomeInterActorImpl interactor);

    CustomApplication provideCustomApplication();

    ActivityNavigator provideActivityNavigator();

}
