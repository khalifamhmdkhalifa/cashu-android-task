package com.example.khalifa.baseproject.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.khalifa.baseproject.R;
import com.example.khalifa.baseproject.model.entity.Repo;
import com.example.khalifa.baseproject.view.interfaces.PaginationListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReposAdapter extends BaseRecyclerViewAdapter<Repo, ReposAdapter.ViewHolder> {

    private PaginationListener paginationListener;

    public ReposAdapter(Context context, List<Repo> items, PaginationListener paginationListener) {
        super(context, items);
        this.paginationListener = paginationListener;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        if (getItemCount() - 2 == position)
            onScrolledToEnd();
    }

    protected void onScrolledToEnd() {
        if (paginationListener != null) {
            paginationListener.onScrolledToEnd();
        }
    }

    @Override
    int getItemLayoutId(int viewType) {
        return R.layout.list_item_repo;
    }

    @Override
    ViewHolder createHolder(View rootView, int viewType) {
        return new ViewHolder(rootView);
    }

    @Override
    void onBindViewHolder(ViewHolder holder, Repo item, int position) {
        holder.textViewRepoTitle.setText(item.getTitle());
        holder.textViewRepoDesc.setText(item.getDescription());
    }

    @Override
    void onItemClicked(Repo item, int position) {

    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textView_RepoTitle)
        TextView textViewRepoTitle;
        @BindView(R.id.textView_RepoDescription)
        TextView textViewRepoDesc;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
