package com.example.khalifa.baseproject.view.interfaces;

import android.arch.lifecycle.ViewModel;
import android.content.Context;

public interface BaseView<VM extends ViewModel> {
    Context getContext();

    void showLoading();

    void hideLoading();

    void showErrorMessage(String message);

    void showNoInternetView();

    void setViewModel(VM viewModel);
}