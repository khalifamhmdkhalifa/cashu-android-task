package com.example.khalifa.baseproject.presenter;

import android.arch.lifecycle.ViewModel;

import com.example.khalifa.baseproject.model.interactor.BaseInterActor;
import com.example.khalifa.baseproject.view.interfaces.BaseFragmentView;

public abstract class BaseFragmentPresenter<V extends BaseFragmentView,
        I extends BaseInterActor, VM extends ViewModel>
        extends BasePresenter<V, I, VM> {
    public BaseFragmentPresenter(V view) {
        super(view);
    }
}
