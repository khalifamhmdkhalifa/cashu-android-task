package com.example.khalifa.baseproject.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import java.util.List;

public abstract class BaseRecyclerViewPaginationAdapter<T extends Object
        , H extends RecyclerView.ViewHolder> extends BaseRecyclerViewAdapter<T, H> {

    public BaseRecyclerViewPaginationAdapter(Context context, List<T> items) {
        super(context, items);
    }

    @Override
    public void onBindViewHolder(H holder, int position) {
        super.onBindViewHolder(holder, position);
        if (getItemCount() == position - 1)
            onScrolledToEnd();
    }

    protected abstract void onScrolledToEnd();
}
