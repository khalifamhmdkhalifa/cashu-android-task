package com.example.khalifa.baseproject.dependencyInjection.component;

import com.example.khalifa.baseproject.dependencyInjection.PerActivity;
import com.example.khalifa.baseproject.dependencyInjection.module.HomeActivityModule;
import com.example.khalifa.baseproject.presenter.HomeActivityPresenter;
import com.example.khalifa.baseproject.view.activities.HomeActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = HomeActivityModule.class)
public interface HomeActivityComponent extends ActivityComponent {
    void inject(HomeActivity homeActivity);

    HomeActivityPresenter providHomeActivityPresenter();
}
