package com.example.khalifa.baseproject.view.interfaces;

import android.arch.lifecycle.ViewModel;

public interface PaginationView<VM extends ViewModel> extends BaseView<VM> {
    void showLoadMoreLoadingView();

    void hideLoadMoreLoadingView();
}
