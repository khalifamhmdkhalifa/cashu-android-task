package com.example.khalifa.baseproject.dependencyInjection.module;

import com.example.khalifa.baseproject.CustomApplication;
import com.example.khalifa.baseproject.model.interactor.HomeInterActor;
import com.example.khalifa.baseproject.model.interactor.HomeInterActorImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class InterActorsModule {

    @Singleton
    @Provides
    HomeInterActor provideHomeInterActor(CustomApplication customApplication) {
        HomeInterActorImpl interactor = new HomeInterActorImpl();
        customApplication.getApplicationComponent().inject(interactor);
        return interactor;
    }
}
