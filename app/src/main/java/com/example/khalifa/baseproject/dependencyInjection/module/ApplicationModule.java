package com.example.khalifa.baseproject.dependencyInjection.module;

import com.example.khalifa.baseproject.CustomApplication;
import com.example.khalifa.baseproject.view.activities.ActivityNavigator;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {
    private final CustomApplication application;

    public ApplicationModule(CustomApplication application) {
        this.application = application;
    }

    @Provides
    public CustomApplication provideCustomApplication() {
        return application;
    }

    @Provides
    public ActivityNavigator provideActivityNavigator() {
        return new ActivityNavigator();
    }
}
