package com.example.khalifa.baseproject.presenter;

import android.content.Intent;

import com.example.khalifa.baseproject.model.interactor.HomeInterActor;
import com.example.khalifa.baseproject.view.interfaces.HomeView;
import com.example.khalifa.baseproject.view.viewModel.HomeViewModel;

public abstract class HomeActivityPresenter extends
        BaseActivityPresenter<HomeView, HomeInterActor, HomeViewModel> {

    public HomeActivityPresenter(HomeView view) {
        super(view);
    }

    public abstract void onReposScrolledToEnd();
}
