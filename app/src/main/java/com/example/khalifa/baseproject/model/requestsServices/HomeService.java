package com.example.khalifa.baseproject.model.requestsServices;

import com.example.khalifa.baseproject.model.entity.Repo;

import java.util.ArrayList;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface HomeService {
    String ENDPOINT = "users/JakeWharton/repos";
    String PER_PAGE_KEY = "perPage";
    String PAGE_NUMBER_KEY = "page";

    @GET(ENDPOINT)
    Observable<ArrayList<Repo>> reposList(@Query(PER_PAGE_KEY) int perPage,
                                          @Query(PAGE_NUMBER_KEY) int page);
}
