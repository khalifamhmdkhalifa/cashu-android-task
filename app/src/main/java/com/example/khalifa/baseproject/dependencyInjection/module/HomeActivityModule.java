package com.example.khalifa.baseproject.dependencyInjection.module;

import android.app.Activity;

import com.example.khalifa.baseproject.CustomApplication;
import com.example.khalifa.baseproject.dependencyInjection.PerActivity;
import com.example.khalifa.baseproject.presenter.HomeActivityPresenter;
import com.example.khalifa.baseproject.presenter.HomeActivityPresenterImpl;
import com.example.khalifa.baseproject.view.interfaces.HomeView;

import dagger.Module;
import dagger.Provides;

@Module
public class HomeActivityModule extends BaseActivityModule {

    public HomeActivityModule(Activity activity) {
        super(activity);
    }

    @Provides
    @PerActivity
    HomeView provideMainView(Activity activity) {
        return (HomeView) activity;
    }

    @Provides
    @PerActivity
    HomeActivityPresenter provideHomeActivityPresenter(
            HomeView homeView, CustomApplication application) {
        HomeActivityPresenterImpl mainActivityPresenter = new HomeActivityPresenterImpl(homeView);
        application.getApplicationComponent().inject(mainActivityPresenter);
        return mainActivityPresenter;
    }
}
