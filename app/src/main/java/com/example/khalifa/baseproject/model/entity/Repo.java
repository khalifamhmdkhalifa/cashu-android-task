package com.example.khalifa.baseproject.model.entity;

import com.google.gson.annotations.SerializedName;

public class Repo {
    @SerializedName("name")
    private String title;
    @SerializedName("description")
    private String description;

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

}
