package com.example.khalifa.baseproject.presenter;

import com.example.khalifa.baseproject.model.entity.Repo;
import com.example.khalifa.baseproject.view.interfaces.BasePaginationViewSubscriber;
import com.example.khalifa.baseproject.view.interfaces.BaseViewSubscriber;
import com.example.khalifa.baseproject.view.interfaces.HomeView;

import java.util.ArrayList;

public class HomeActivityPresenterImpl extends HomeActivityPresenter {
    private int currentPageNumber = 0;
    private boolean isLoading = false;
    private boolean isEndReached = false;

    public HomeActivityPresenterImpl(HomeView view) {
        super(view);
    }

    @Override
    public void onReposScrolledToEnd() {
        if (isEndReached || isLoading)
            return;
        loadNextPage();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        loadRepos();
    }

    private void loadRepos() {
        isEndReached = false;
        isLoading = true;
        subscribeObservable(
                getReposSubscriber(), getInterActor().getRepos(1));
    }

    private void loadNextPage() {
        isLoading = true;
        subscribeObservable(getLoadMoreReposSubscriber()
                , getInterActor().getRepos(currentPageNumber + 1));
    }

    private BaseViewSubscriber getReposSubscriber() {
        return new BaseViewSubscriber<ArrayList<Repo>>(getView(),
                true, false) {
            @Override
            public void onSuccess(ArrayList<Repo> result) {
                handleReposResponseSuccess(result);
            }

            @Override
            protected void onError(Throwable throwable) {
                super.onError(throwable);
                handleReposResponseError();
            }
        };
    }

    private BaseViewSubscriber getLoadMoreReposSubscriber() {
        return new BasePaginationViewSubscriber<ArrayList<Repo>>(getView(),
                true, false) {
            @Override
            public void onSuccess(ArrayList<Repo> result) {
                handleReposResponseSuccess(result);
            }

            @Override
            protected void onError(Throwable throwable) {
                super.onError(throwable);
                handleReposResponseError();
            }
        };
    }

    private void handleReposResponseSuccess(ArrayList<Repo> result) {
        isLoading = false;
        if (result == null)
            return;
        if (result.isEmpty()) {
            isEndReached = true;
            return;
        }
        currentPageNumber++;
        if (currentPageNumber == 1)
            viewModel.getCurrentRepos().postValue(result);
        else
            appendReposToCurrentList(result);
    }

    private void appendReposToCurrentList(ArrayList<Repo> newRepos) {
        ArrayList<Repo> currentList = viewModel.getCurrentRepos().getValue();
        if (currentList == null || !isViewAttached())
            return;
        currentList.addAll(newRepos);
        getView().notifyReposListChanged();
    }

    private void handleReposResponseError() {
        isLoading = false;
        if (isViewAttached()) {
            getView().showFailedToSendRequestMessage();
        }
    }
}
