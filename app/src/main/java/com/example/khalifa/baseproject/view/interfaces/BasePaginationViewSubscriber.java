package com.example.khalifa.baseproject.view.interfaces;

import io.reactivex.disposables.Disposable;

public abstract class BasePaginationViewSubscriber<T> extends BaseViewSubscriber<T> {
    PaginationView paginationView;

    public BasePaginationViewSubscriber(PaginationView view
            , boolean showLoading, boolean showErrorMessage) {
        super(view, showLoading, showErrorMessage);
        this.paginationView = view;
    }


    @Override
    protected void onError(Throwable throwable) {
        throwable.printStackTrace();
        if (showLoading)
            paginationView.hideLoadMoreLoadingView();
        if (showErrorMessage)
            view.showErrorMessage(throwable.getMessage());
    }

    @Override
    protected void onSubscribe(Disposable disposable) {
        if (showLoading)
            paginationView.showLoadMoreLoadingView();
    }

    @Override
    protected void onComplete() {
        if (showLoading)
            paginationView.hideLoadMoreLoadingView();
    }
}
