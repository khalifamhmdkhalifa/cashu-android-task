package com.example.khalifa.baseproject.view.interfaces;

import com.example.khalifa.baseproject.view.viewModel.HomeViewModel;

public interface HomeView extends BaseActivityView<HomeViewModel>,PaginationView<HomeViewModel> {
    void showFailedToSendRequestMessage();

    void notifyReposListChanged();
}
