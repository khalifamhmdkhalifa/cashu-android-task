package com.example.khalifa.baseproject.view.interfaces;

public interface PaginationListener {
    void onScrolledToEnd();
}
