package com.example.khalifa.baseproject.presenter;

import android.arch.lifecycle.ViewModel;
import android.content.Intent;

import com.example.khalifa.baseproject.model.interactor.BaseInterActor;
import com.example.khalifa.baseproject.view.interfaces.BaseActivityView;

public abstract class BaseActivityPresenter<V extends BaseActivityView
        , I extends BaseInterActor, VM extends ViewModel> extends BasePresenter<V, I, VM> {

    public BaseActivityPresenter(V view) {
        super(view);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    }
}