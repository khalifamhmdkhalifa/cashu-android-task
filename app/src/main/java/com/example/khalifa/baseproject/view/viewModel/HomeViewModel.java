package com.example.khalifa.baseproject.view.viewModel;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import com.example.khalifa.baseproject.model.entity.Repo;

import java.util.ArrayList;


public class HomeViewModel extends ViewModel {
    private MutableLiveData<ArrayList<Repo>> repos;

    public MutableLiveData<ArrayList<Repo>> getCurrentRepos() {
        if (repos == null) {
            repos = new MutableLiveData();
        }
        return repos;
    }

}
