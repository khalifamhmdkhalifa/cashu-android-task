package com.example.khalifa.baseproject.model.interactor;

import com.example.khalifa.baseproject.model.entity.Repo;

import java.util.ArrayList;

import io.reactivex.Observable;

public abstract class HomeInterActor extends BaseNetworkInterActor {
    public abstract Observable<ArrayList<Repo>> getRepos(int pageNumber);
}