package com.example.khalifa.baseproject.dependencyInjection.module;

import com.example.khalifa.baseproject.view.viewModel.HomeViewModel;

import dagger.Module;
import dagger.Provides;

@Module
public class ViewModelModule {

    @Provides
    HomeViewModel provideHomeViewModel() {
        return new HomeViewModel();
    }
}
