package com.example.khalifa.baseproject.view.activities;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.example.khalifa.baseproject.R;
import com.example.khalifa.baseproject.dependencyInjection.component.DaggerHomeActivityComponent;
import com.example.khalifa.baseproject.dependencyInjection.component.HomeActivityComponent;
import com.example.khalifa.baseproject.dependencyInjection.module.HomeActivityModule;
import com.example.khalifa.baseproject.model.entity.Repo;
import com.example.khalifa.baseproject.presenter.HomeActivityPresenter;
import com.example.khalifa.baseproject.view.adapter.ReposAdapter;
import com.example.khalifa.baseproject.view.interfaces.HomeView;
import com.example.khalifa.baseproject.view.viewModel.HomeViewModel;

import java.util.ArrayList;

import butterknife.BindView;

public class HomeActivity extends BaseActivityImplement<HomeActivityPresenter,
        HomeActivityComponent, HomeViewModel> implements HomeView {
    @BindView(R.id.recyclerView_repos)
    RecyclerView recyclerViewRepos;
    @BindView(R.id.view_loadMore)
    View viewLoadMore;
    private ReposAdapter reposAdapter;


    @Override
    HomeActivityComponent initComponent() {
        return DaggerHomeActivityComponent.builder().homeActivityModule(
                new HomeActivityModule(this))
                .applicationComponent(getApplicationComponent()).build();
    }

    @Override
    void inject() {
        getActivityComponent().inject(this);
    }

    @Override
    protected int getLoadingViewId() {
        return R.id.view_loadingView;
    }

    @Override
    protected int getLayoutId() {
        return (R.layout.activity_home);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onViewModelAttached() {
        super.onViewModelAttached();
        getViewModel().getCurrentRepos().observe(this, (newList) -> {
            initReposAdapter(newList);
            setupRecyclerView();
        });
    }

    @Override
    public void showFailedToSendRequestMessage() {
        Toast.makeText(this, R.string.failed_request_message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void notifyReposListChanged() {
        if (reposAdapter != null)
            reposAdapter.notifyDataSetChanged();
    }

    private void setupRecyclerView() {
        recyclerViewRepos.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewRepos.setAdapter(reposAdapter);
    }

    private void initReposAdapter(ArrayList<Repo> repos) {
        reposAdapter = new ReposAdapter(this, repos, getPresenter()::onReposScrolledToEnd);
    }

    @Override
    public void showLoadMoreLoadingView() {
        viewLoadMore.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadMoreLoadingView() {
        viewLoadMore.setVisibility(View.GONE);
    }
}